﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Exchange.WebServices.Data;
using Microsoft.SharePoint;
using System.Net;
using System.DirectoryServices;
using System.Data.SqlClient;
using System.Data;
using Massive;
using System.Text.RegularExpressions;

namespace outofoffice
{
    class Program
    {
        static void Main(string[] args)
        {
            //test
            DateTime now = DateTime.Now; // get the time now so we can calculate elapsed time later
            //Create the exchange web service binding
            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010);
            service.Credentials = new NetworkCredential("keymaster", "sadfart44", "EMAAD");
            service.Url = new Uri("https://email.mower.com/EWS/Exchange.asmx");

            ExchangeService service2 = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
            service2.Credentials = new NetworkCredential("keymaster@mower.com", "sadfart44", "EMAAD");
            service2.Url = new Uri("https://outlook.office365.com/ews/exchange.asmx");

            // Create a list where we will put the objects we want to insert into the database.
            // This way we can delete everything and immediately insert the new records, meaning less chance of
            // a user accessing the database while it is being populated.
            HashSet<dynamic> objectsToInsert = new HashSet<dynamic>();

            // retStr holds any errors that happen while the task is running.  These errors will appear in Telligent's exception manager
            string retStr = "The Out of Office database refresh completed successfully.  Following are any errors that were noticed during the procedure: \r\n";
            string temporaryErrorHolder = ""; // This lets us see if there are any errors before placing the error headers in the error string
            // populate objectsToInsert with the OOF events from each office

            //THIS SECTION WILL EVENTUALLY ALLOF FOR ACCESS TO 365 ACCOUNTS
            temporaryErrorHolder = updateOOF("LDAP://DC=mower,DC=com", service2, objectsToInsert); // service 2 connects to office 365 vs internal
            if (temporaryErrorHolder != "")
            {
                retStr += "Office 365 OOF errors: \r\n";
                retStr += temporaryErrorHolder;
            }

            /*
            temporaryErrorHolder = updateOOF("LDAP://DC=syr,DC=mower,DC=com", service, objectsToInsert); // The DC=syr is used to determine the office id for each person in the query
            if (temporaryErrorHolder != "")
            {
                retStr += "Syracuse OOF errors: \r\n";
                retStr += temporaryErrorHolder;
            }
            temporaryErrorHolder = updateOOF("LDAP://DC=buf,DC=mower,DC=com", service, objectsToInsert);
            if (temporaryErrorHolder != "")
            {
                retStr += "Buffalo OOF errors: \r\n";
                retStr += temporaryErrorHolder;
            }
            temporaryErrorHolder = updateOOF("LDAP://DC=atl,DC=mower,DC=com", service, objectsToInsert);
            if (temporaryErrorHolder != "")
            {
                retStr += "Atlanta OOF errors: \r\n";
                retStr += temporaryErrorHolder;
            }
            temporaryErrorHolder = updateOOF("LDAP://DC=alb,DC=mower,DC=com", service, objectsToInsert);
            if (temporaryErrorHolder != "")
            {
                retStr += "Albany OOF errors: \r\n";
                retStr += temporaryErrorHolder;
            }
            temporaryErrorHolder = updateOOF("LDAP://DC=roc,DC=mower,DC=com", service, objectsToInsert);
            if (temporaryErrorHolder != "")
            {
                retStr += "Rochester OOF errors: \r\n";
                retStr += temporaryErrorHolder;
            }
            temporaryErrorHolder = updateOOF("LDAP://DC=clt,DC=mower,DC=com", service, objectsToInsert);
            if (temporaryErrorHolder != "")
            {
                retStr += "Charlotte OOF errors: \r\n";
                retStr += temporaryErrorHolder;
            }
            temporaryErrorHolder = updateOOF("LDAP://DC=mower,DC=com", service, objectsToInsert);

            if (temporaryErrorHolder != "")
            {
                retStr += "Cincy OOF errors: \r\n";
                retStr += temporaryErrorHolder;
            }
            */
            //delete everything and get/insert again
            deleteAllOOF();
            DynamicModel sqlConnection = new DynamicModel("outofoffice", "OutOfOffice", "Id"); // connect to the OOF database
            foreach (dynamic queryObject in objectsToInsert) // This iterates through each unique OOF entry
            {
                sqlConnection.Insert(queryObject); // This data is now in the Telligent database in the OutOfOffice table
            }
            string timeDifference = DateTime.Now.Subtract(now).TotalSeconds.ToString(); // figure out how long this script took to run


            if (retStr != "") // if there were any errors
            {
                retStr += "This script took " + timeDifference + " seconds to run.";
                Console.Write(retStr);
                //Console.ReadLine();
                //throw new Exception(retStr); // This can be viewed from the Telligent exceptions manager
            }
            // these lines would send an email if you wanted to email out the error messages from the server
            //SendMail m = new SendMail();
            //m.sendMail(ConfigurationManager.AppSettings["error_from"].ToString(), ConfigurationManager.AppSettings["error_to"].ToString(), strErr, "OOF refresh is complete", "", true);
        }
        static public string deleteAllOOF() // deletes all OOF in the db
        {
            DynamicModel sqlConnection = new DynamicModel("outofoffice", "OutOfOffice", "Id"); // connect to the OOF database
            string strErr = "";
            sqlConnection.Delete(); // Destroy everything
            return strErr;
        }

        static public string updateOOF(string strLDAP, ExchangeService service, HashSet<dynamic> objectsToInsert) // adds fresh OOF messages into the db
        {
            string retVal = ""; // This will hold any errors we find

            DirectoryEntry de = new DirectoryEntry(strLDAP); // This is an AD object necessary to retrieve OOF messages
            de.Username = "keymaster";
            de.Password = "sadfart44";

            //This block prints out all available properties retrieved by the directory searcher
            /*if (strLDAP == "LDAP://DC=syr,DC=mower,DC=com")
            {
                DirectoryEntry testEntry = new DirectoryEntry(strLDAP);
                DirectorySearcher adSearcher = new DirectorySearcher(testEntry);
                adSearcher.SearchScope = SearchScope.Subtree;
                adSearcher.Filter = "(mail=kstaruck@mower.com)";
                SearchResult srs = adSearcher.FindOne();

                foreach (string propName in srs.Properties.PropertyNames)
                {
                    retVal += propName + ": " + srs.Properties[propName][0].ToString() + "<br />";
                }
            }
            */

            DirectorySearcher ds = new DirectorySearcher(de, "(objectClass=user)"); // This is another AD object necessary to retrieve OOF messages
            ds.Filter = "(givenname>=AAAAA)"; // Don't return people whose names start with numbers?
            ds.PropertiesToLoad.Add("name"); // Full name
            ds.PropertiesToLoad.Add("givenname");  // First name
            ds.PropertiesToLoad.Add("samaccountname"); // first initial of first name, last name: dbulger
            ds.PropertiesToLoad.Add("sn"); // Surname (last name)
            ds.PropertiesToLoad.Add("mail"); // Primary email address
            //LEE - remove AD department
            //ds.PropertiesToLoad.Add("department"); // Agency department
            ds.PropertiesToLoad.Add("l"); // Office (syracuse, buffalo...)
            SearchResultCollection src = ds.FindAll();  // This is a collection of search results that contains the employees we need
            List<AttendeeInfo> attendees = new List<AttendeeInfo>();  // This will hold the attendees we found so we can do a GetUserAvailability call
            List<Dictionary<string, string>> people = new List<Dictionary<string, string>>(); // This is a list of dictionaries that hold properties for a certain employee
            //string officeName = Regex.Replace(strLDAP, "LDAP://DC=", "").Split(',')[0]; // Gets the domain of search, which is the office location of every user
            string officeName = "";
            if (strLDAP == "LDAP://DC=mower,DC=com")
            {
                officeName = "cin";
            }
            else
            {
                officeName = Regex.Replace(strLDAP, "LDAP://DC=", "").Split(',')[0]; // Gets the domain of search, which is the office location of every user
            }
            foreach (SearchResult result in src)
            {
                if (result.Properties.Contains("mail")) // We only care about a result if the person has an email address.  No entity without an email address should have relevant OOF 
                {
                    Dictionary<string, string> person = new Dictionary<string, string>(); // Create a new person
                    /*LEE - Remove AD department
                    if (result.Properties.Contains("department"))
                    { // If the employee has a department defined, add it to the person object
                        person["department"] = result.Properties["department"][0].ToString();
                    }
                    else
                    {
                        person["department"] = "Not specified"; // If this happens, hopefully MILKY_WAY can tell us the department
                    }
                    if (result.Properties["mail"][0].ToString().ToLower()=="dbrim@mower.com")
                    { Console.WriteLine(result.Properties["mail"][0].ToString().ToLower()); }*/
                    person["mail"] = result.Properties["mail"][0].ToString();
                    person["name"] = result.Properties["name"][0].ToString();
                    person["firstname"] = result.Properties["givenname"][0].ToString();
                    if (result.Properties["sn"].Count > 0)
                    { // If the employee has a last name (surname) defined, add it to the person object
                        person["lastname"] = result.Properties["sn"][0].ToString();
                    }
                    else
                    {
                        person["lastname"] = person["name"]; // This really should never happen, but we want to make sure lastname is defined anyway
                    }
                    person["location"] = officeName;
                    people.Add(person); // Add this person to our list of people

                    attendees.Add(new AttendeeInfo(person["mail"])); // Add this person's email address to our list of email addresses to use in our search

                }
            }
            AvailabilityOptions myOptions = new AvailabilityOptions(); // Options for our GetUserAvailability search
            myOptions.RequestedFreeBusyView = FreeBusyViewType.Detailed; // Specifies we want detailed availability info
            int numAttendees = attendees.Count; // How many people exist
            int numFullCycles = (int)Math.Floor((numAttendees) / 100.0); // How many full sets of 100 people exist
            int extra = numAttendees % 100; // How many people are left when all sets of 100 people have been searched
            GetUserAvailabilityResults freeBusyResults; // results will go here

            for (int i = 0; i <= numFullCycles; i++) // Since the GetUserAvailability call limits to 100 people per search, we need to do some math to get them all in
            {
                /* math is happening */
                int amountLeft;
                if (i < numFullCycles) // if we still have a full set of 100 people left
                {
                    amountLeft = 100; // analyze next 100 people
                }
                else
                {
                    amountLeft = extra; // analyze the last people
                }
                if (amountLeft == 0) // edge case when # of people is a multiple of 100
                {
                    break; // don't analyze 0 people.
                }
                for (int dayOffset = 0; dayOffset <= 372; dayOffset += 62)
                { // we can only get availability within a two-month period, so we will need to do some looping.
                    freeBusyResults = service.GetUserAvailability(attendees.GetRange(i * 100, amountLeft),
                        new TimeWindow(DateTime.Now.AddDays(-7).AddDays(dayOffset), DateTime.Now.AddDays(55).AddDays(dayOffset)),
                        AvailabilityData.FreeBusy,
                        myOptions); // Do the GetUserAvailability search within the current 2 month period
                    int j = 0; // counter variable since we won't use a for loop
                    foreach (AttendeeAvailability availability in freeBusyResults.AttendeesAvailability)
                    {
                        int currentIndex = j + i * 100; // The index in our people array that corresponds to the person whom we are currently examining
                        Dictionary<string, string> currentPerson = people[currentIndex]; // The person whose OOF messages we are examining
                        bool consultedTheMilkyWay = false; // Only check Milky Way once per person, and only if that person has OOF.
                        //LEE - remove AD department
                        //string strDepartment = currentPerson["department"]; // The department is special because the data we find in Milky Way can override the data we obtained from Active Directory
                        string strDepartment = "";
                        string strOfficeID = "";
                        foreach (CalendarEvent calendarItem in availability.CalendarEvents)
                        {
                            if (calendarItem.FreeBusyStatus.ToString() == "OOF") // If this is indeed an OOF message
                            {
                                // begin DB stuff
                                if (consultedTheMilkyWay == false) // only check this once per person, instead of once per message pertaining to the same person.
                                {
                                    consultedTheMilkyWay = true; // only check once
                                    // LIVE CHANGE
                                    /* Change this to the live MILKY_WAY when moving to live*/
                                    SqlConnection conn = new SqlConnection("Data Source=EMA-SQL3;Initial Catalog=MILKY_WAY;User ID=supersql;Password=flavRf1@v");
                                    /********************************************************/
                                    // LIVE CHANGE
                                    /* Remember to change the stored proc on live to filter out the special individuals */
                                    SqlCommand command = new SqlCommand("sp_GetEmpInfoEmailMoreInfoFilter", conn); // run the stored procedure to get employee info
                                    /********************************************************/
                                    command.CommandType = CommandType.StoredProcedure;
                                    command.Parameters.Add("@email", SqlDbType.VarChar).Value = currentPerson["mail"].Replace("'", ""); // pass in the current employee's email address without quotes (MILKY_WAY entries do not have quotes)
                                    conn.Open();
                                    try
                                    {
                                        SqlDataReader reader = command.ExecuteReader();
                                        if (!reader.HasRows)
                                        {
                                            retVal += "No record found in milky_way for " + currentPerson["name"] + " with email address " + currentPerson["mail"].Replace("'", "") + "\r\n";
                                            break; // If no rows are returned, this individual either does not exist in Milky_Way or is a special person with inaccessible data, so we don't take any OOF into account for this person
                                        }
                                        if (reader.Read())
                                        {
                                            strDepartment = (reader["DP_TM_DESC"].ToString() != null) ? reader["DP_TM_DESC"].ToString() : strDepartment; // If Milky Way has data for us, we will use it instead of the AD data
                                            strOfficeID = (reader["OFFICES_ID"].ToString() != null) ? reader["OFFICES_ID"].ToString() : strOfficeID;
                                        }
                                    }
                                    catch (Exception ex) // if something goes wrong, spit it into the return stream, don't break.
                                    {
                                        retVal += "\r\n userOOF() inner loop, get: email: " + currentPerson["mail"] + "\r\n Message: " + ex.Message.ToString() + "\r\n Source: " + ex.Source.ToString() + "\r\n";
                                    }
                                    conn.Close();
                                }
                                //JAH 7/10/13 changed to use the office information from ADP in Milkyway vs. Active Directory which is no longer valid.
                                if (strOfficeID == "")
                                {
                                    Dictionary<string, string> officeMap = new Dictionary<string, string>(); // This maps the three-letter abbreviations for offices to their office ids.
                                    officeMap["syr"] = "4";
                                    officeMap["buf"] = "3";
                                    officeMap["atl"] = "2";
                                    officeMap["alb"] = "1";
                                    officeMap["clt"] = "7";
                                    officeMap["roc"] = "6";
                                    officeMap["cin"] = "10"; //LEE - add cincy
                                    officeMap["nyc"] = "11";

                                    strOfficeID = officeMap[currentPerson["location"]]; // set the current office ID
                                }
                                else
                                {

                                    switch (strOfficeID)
                                    {
                                        case "4":
                                            currentPerson["location"] = "syr";
                                            break;
                                        case "3":
                                            currentPerson["location"] = "buf";
                                            break;
                                        case "2":
                                            currentPerson["location"] = "atl";
                                            break;
                                        case "1":
                                            currentPerson["location"] = "alb";
                                            break;
                                        case "7":
                                            currentPerson["location"] = "clt";
                                            break;
                                        case "6":
                                            currentPerson["location"] = "roc";
                                            break;
                                        case "10":
                                            currentPerson["location"] = "cin";
                                            break;
                                        case "11":
                                            currentPerson["location"] = "nyc";
                                            break;
                                        case "12":
                                            currentPerson["location"] = "bos";
                                            break;
                                        default:
                                            currentPerson["location"] = "syr";
                                            break;
                                    }

                                }
                                string appointmentLocation = "No location given.";
                                string appointmentDescription = "No description given.";
                                if (calendarItem.Details != null)
                                {
                                    if (calendarItem.Details.IsPrivate)
                                    {
                                        appointmentLocation = "This appointment is private.";
                                        appointmentDescription = "This appointment is private.";
                                    }
                                    else
                                    {
                                        try
                                        {
                                            appointmentLocation = calendarItem.Details.Location.Substring(1, 99);
                                        }
                                        catch
                                        {
                                            appointmentLocation = "No location given.";
                                        }
                                        appointmentDescription = calendarItem.Details.Subject;
                                    }
                                }
                                objectsToInsert.Add(new
                                {
                                    StartTime = calendarItem.StartTime,
                                    EndTime = calendarItem.EndTime,
                                    Location = appointmentLocation,
                                    Description = appointmentDescription,
                                    Name = currentPerson["name"],
                                    FirstName = currentPerson["firstname"],
                                    LastName = currentPerson["lastname"],
                                    Email = currentPerson["mail"].Replace("'", "").ToLower(),
                                    OfficeName = currentPerson["location"],
                                    OfficeId = strOfficeID,
                                    Department = strDepartment
                                }); // Create the object that will be inserted into the OOF database
                            }
                        }
                        j++;
                    }
                }
            }
            return retVal; // If we want to print errors, we can run this function and print its return value
        }
    }
}
